# Deploy 1

Aqui nós temos uma primeira versão do projeto e o ambiente que vamos usar para disponibilizar as versões para nossos clientes, porém precisamos realizar o deploy da aplicação no nosso servidor, siga as instruções abaixo para prepararmos o ambiente e atualizar nossa aplicação em produção.

## Pré-requisitos

- Docker instalado.
- Git instalado.

### Setup

Para viabilizar o exercício nós iremos utilizar servidores Linux usando containers docker e simular todo o ambiente de produção local.

Os domínios utilizados serão `devops.prod`, `app.devops.prod`, `admin.devops.prod` e para configurá-los edite o arquivo `/etc/hosts` e adicione a linha a seguir:

```
127.0.0.1 devops.prod
127.0.0.1 admin.devops.prod
127.0.0.1 app.devops.prod
```

Baixe o projeto `cpf_validator_api` pelo git.

> git clone https://gitlab.com/pigor/cpf_validator_api.git

### Informações da Aplicação

O sistema desenvolvido é uma API que retorna se o CPF é válido ou não. É uma aplicação simples que não utiliza banco de dados e nenhuma outra integração e precisa basicamente de um servidor web para disponibilizar o serviço.

Esse sistema foi construído usando NodeJs (https://nodejs.org/en/) e Express (https://expressjs.com/)

### Exercício

É hora do deploy e precisamos subir um servidor Web (NGINX) e publicar a aplicação lá. Vamos começar preparando nosso servidor.

1.  Vamos subir um servidor Ubuntu (para o exercício vamos usar um container docker para simular) e configurar o acesso via SSH

1.1. Iniciar o container Ubuntu 18.04 com o servidor SSH expondo as portas 80, 22 e 3000.

> sudo docker run -d -P -p 80:80 -p 22:22 -p 3000:3000 -p 3001:3001 --name cpf-server rastasheep/ubuntu-sshd:18.04

1.2. Adicionar a chave SSH pública no servidor para facilitar o acesso SSH.

Se não possuir uma chave SSH execute o comando `ssh-keygen -t rsa` para gerá-la.

> sudo docker cp ~/.ssh/id_rsa.pub cpf-server:/root/.ssh/authorized_keys

> sudo docker exec cpf-server chown root:root /root/.ssh/authorized_keys

2.  Agora vamos configurar o servidor

2.1. Acessar o servidor via SSH (o host do container é localhost)

> ssh root@devops.prod

2.2. Instalar os pacotes essenciais

> apt-get update

> apt-get install -y git curl build-essential vim

2.3. Instalar o NodeJs

> curl -sL https://deb.nodesource.com/setup_10.x | bash -

> apt-get install -y nodejs

2.4. Instalar o gerenciador de processos NodeJs PM2

> npm install pm2 -g

2.5. Instalar o NGINX

> apt-get install -y nginx

2.6. Configurar o NGINX

> vim /etc/nginx/conf.d/cpf_validator_api.conf

2.7. Adicione o conteúdo abaixo no arquivo `cpf_validator_api.conf`

```
upstream cpf_api {
    server devops.prod:3000;
}

server {
    listen          80;
    server_name     devops.prod;

    location /cpf_api/ {
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_pass http://cpf_api/;
    }
}
```

3.  Clone da aplicação no servidor

> cd /opt

> git clone https://gitlab.com/pigor/cpf_validator_api.git

4.  Inicializar a Aplicação

4.1. Instalar as dependências (execute o comando dentro da pasta onde está a aplicação)

> cd /opt/cpf_validator_api

> npm install

4.2. Iniciar o processo NodeJs (execute o comando dentro da pasta onde está a aplicação)

> pm2 start app.js

5.  Reiniciar o NGINX

> /etc/init.d/nginx restart

6.  Testar se a aplicação ficou disponível acessando `http://devops.prod/cpf_api`

7.  Para testar o validador tente: `http://devops.prod/cpf_api/validator?cpf=<os números de um cpf aqui>`
